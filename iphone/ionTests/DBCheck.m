//
//  DBCheck.m
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "DataManager.h"
#import "Post.h"

@interface DBCheck : XCTestCase

@end

@implementation DBCheck

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testOption {
    
    NSManagedObject *option = [NSEntityDescription insertNewObjectForEntityForName:@"Option"
                                                            inManagedObjectContext:[DataManager sharedManager].managedObjectContext];

    [option setValue:@"YES" forKey:@"name"];
    [option setValue:[NSNumber numberWithInt:0] forKey:@"option_id"];

    NSError *error = nil;
    BOOL ret = [[DataManager sharedManager].managedObjectContext save:&error];
    if (!ret) {
        NSLog(@"savePost failed with error (%@)", error);
    }

    XCTAssert(ret, @"Pass");
}

- (void)testParticipant {

    NSManagedObject *participant = [NSEntityDescription insertNewObjectForEntityForName:@"Participant"
                                                            inManagedObjectContext:[DataManager sharedManager].managedObjectContext];
    
    [participant setValue:@"999999" forKey:@"participant_id"];
    
    NSError *error = nil;
    BOOL ret = [[DataManager sharedManager].managedObjectContext save:&error];
    if (!ret) {
        NSLog(@"savePost failed with error (%@)", error);
    }
    
    XCTAssert(ret, @"Pass");

}

- (void)testSavePost {

    BOOL ret = NO;
    
    NSEntityDescription *entityDesc = [[[DataManager sharedManager].managedObjectModel entitiesByName] valueForKey:@"Participant"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"participant_id = 999999"];
    NSFetchRequest *request = [[NSFetchRequest alloc ] init];
    [request setEntity:entityDesc];
    [request setPredicate:predicate];
    
    NSArray *participants = [[DataManager sharedManager].managedObjectContext executeFetchRequest:request error:nil];
    if (participants && [participants count] > 0) {
        
        NSManagedObject *post = [NSEntityDescription insertNewObjectForEntityForName:@"Post"
                                                              inManagedObjectContext:[DataManager sharedManager].managedObjectContext];
        [post setValue:@"this is a unit test of post scheme" forKey:@"text"];
        [post setValue:[NSDate date] forKey:@"expiration"];
        [post setValue:@"xijalksdjfl" forKey:@"post_id"];
        [post setValue:[NSNumber numberWithInt:(POST_STATUS_DRAFT | MY_POST_MASK)] forKey:@"status"];
        
        [post setValue:[participants firstObject] forKey:@"issued_by"];
        
        NSError *error = nil;
        ret = [[DataManager sharedManager].managedObjectContext save:&error];
        if (!ret) {
            NSLog(@"savePost failed with error (%@)", error);
        }
    }
    
    XCTAssert(ret, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
