//
//  ViewController.m
//  ion
//
//  Created by Lawrence on 7/10/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
            

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self performSelector:@selector(goToLoginView) withObject:nil afterDelay:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goToLoginView {
    NSLog(@"%f", [[NSDate date] timeIntervalSince1970]);
    [self performSegueWithIdentifier:@"go2LoginView" sender:self];
}

@end
