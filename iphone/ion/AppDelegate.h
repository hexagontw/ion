//
//  AppDelegate.h
//  ion
//
//  Created by Lawrence on 7/10/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

