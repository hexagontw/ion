//
//  Response.h
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Option, Participant, Post;

@interface Response : NSManagedObject

@property (nonatomic, retain) Participant *answered_by;
@property (nonatomic, retain) Post *in_post;
@property (nonatomic, retain) Option *is_option;

@end
