//
//  Option.h
//  ion
//
//  Created by Lawrence on 8/11/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post, Response;

@interface Option : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * obj_id;
@property (nonatomic, retain) NSSet *answered_by;
@property (nonatomic, retain) NSSet *used_by;
@end

@interface Option (CoreDataGeneratedAccessors)

- (void)addAnswered_byObject:(Response *)value;
- (void)removeAnswered_byObject:(Response *)value;
- (void)addAnswered_by:(NSSet *)values;
- (void)removeAnswered_by:(NSSet *)values;

- (void)addUsed_byObject:(Post *)value;
- (void)removeUsed_byObject:(Post *)value;
- (void)addUsed_by:(NSSet *)values;
- (void)removeUsed_by:(NSSet *)values;

@end
