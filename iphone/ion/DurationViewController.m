//
//  DurationViewController.m
//  ion
//
//  Created by Lawrence on 7/17/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "DurationViewController.h"

#define DEFAULT_MINMUM_INTERVAL     15

@interface DurationViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation DurationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setMinimumDate:2];
    [self updateDurationLabel];
    [self.timePicker addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(valueChanged:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setMinimumDate:(int)times
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    NSDateComponents *components = [gregorian components: (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit)
                                                fromDate:[NSDate dateWithTimeIntervalSinceNow:times*DEFAULT_MINMUM_INTERVAL*60]];
    components.minute -= (components.minute % DEFAULT_MINMUM_INTERVAL);
    components.second = 0;
    [self.timePicker setMinimumDate:[gregorian dateFromComponents:components]];
    
}

- (void)updateDurationLabel {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *components = [gregorian components:unitFlags
                                                fromDate:[NSDate date]
                                                  toDate:[self.timePicker date]
                                                 options:0];
    if (components.day == 0 && components.hour == 0 && components.minute && components.second <=0) {
        [self setMinimumDate:1];
        [self valueChanged:self];
    } else {
        self.durationLabel.text = [NSString stringWithFormat:@"%ld Day(s) %02ld:%02ld:%02ld", components.day, components.hour, components.minute, components.second];
    }
}

- (void)valueChanged:(id)sender {
    [self updateDurationLabel];
}

#pragma mark -
#pragma mark PrepareForSegue
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"UnwindDatePicker"]) {
        
        
        return YES;
    }
    
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"UnwindDatePicker"]) {
        
    } else {
        // TODO: fault tolerance
        NSLog(@"invalid segue");
    }
}

@end
