//
//  EnterInfoViewController.h
//  ion
//
//  Created by Lawrence on 7/24/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

- (IBAction)ok:(id)sender;
- (IBAction)skip:(id)sender;

@end
