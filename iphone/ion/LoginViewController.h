//
//  LoginViewController.h
//  ion
//
//  Created by Lawrence on 7/13/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUser;
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UILabel *loginInfo;

- (IBAction)signUp:(id)sender;

@end
