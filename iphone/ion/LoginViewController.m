//
//  LoginViewController.m
//  ion
//
//  Created by Lawrence on 7/13/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Parse/Parse.h>
#import "LoginViewController.h"
#import "DataManager.h"
#import "Profile.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSArray *profiles = [[DataManager sharedManager] getProfiles];
    if (profiles && [profiles count]) {
        Profile *profile = [profiles firstObject];
        NSLog(@"set current login user is %@", profile.user_id);
        [[DataManager sharedManager] setCurrentProfileId:profile.user_id];
        [_txtUser setText:profile.user_id];
    }
    
    [_txtPass setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField setTextColor:[UIColor blackColor]];
    self.loginInfo.text = @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _txtUser) {
        [_txtPass becomeFirstResponder];
    }else {
        [self login:textField];
    }
    
    return YES;
}

- (IBAction)login:(id)sender {
    
    
    if ([_txtUser.text length] == 0) {
        [_txtUser becomeFirstResponder];
        return;
    } else if ([_txtPass.text length] == 0) {
//        [_txtPass becomeFirstResponder];
//        return;
        [_txtPass setText:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
#ifdef DEBUG
        [_txtPass setText:@"387086FB-3375-43F9-83E7-48A1FE7BA843"];
#endif
        NSLog(@"set password as %@", [[[UIDevice currentDevice] identifierForVendor] UUIDString]);
    }

    [self.btnLogin setEnabled:NO];
    [self.btnSignup setEnabled:NO];
    [PFUser logInWithUsernameInBackground:_txtUser.text
                                 password:_txtPass.text
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            [[DataManager sharedManager] associateCurrentUsertoInstallation:user];
                                            [self performSegueWithIdentifier:@"go2MainVoteViewFromLogin" sender:self];                                            
                                        } else {
                                            // The login failed. Check error to see why.
                                            self.loginInfo.text = @"Login failed";
                                            NSLog(@"%@", error);
                                            [self.btnLogin setEnabled:YES];
                                            [self.btnSignup setEnabled:YES];
                                            [self performSegueWithIdentifier:@"go2MainVoteViewFromLogin" sender:self];
                                        }
                                    }];
    
    /*
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *type = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    [store requestAccessToAccountsWithType:type
                                   options:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:nil,nil]
                                                                       forKeys:[NSArray arrayWithObjects:ACFacebookAppIdKey, ACFacebookPermissionsKey,nil]]
                                completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             NSArray *arrayOfAccounts = [store accountsWithAccountType:type];
             if ([arrayOfAccounts count] > 0)
             {
                 NSLog(@"%@", arrayOfAccounts);
             }
         };
     }];
     
     */
}

- (IBAction)signUp:(id)sender {



}



@end
