//
//  DataManager.m
//  ion
//
//  Created by Lawrence on 7/20/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Parse/Parse.h>
#import "DataManager.h"

@import AddressBook;
@import CoreTelephony;

#import "Participant.h"
#import "Profile.h"
#import "User.h"
#import "Option.h"
#import "Response.h"
#import "Post.h"
#import "Config.h"
#import "NBPhoneNumberUtil.h"

@interface DataManager ()
{
    NSString *profileId;
}

@property (nonatomic, assign) ABAddressBookRef addressBook;

- (NSString*)getCountryCode;

@end

@implementation DataManager

SYNTHESIZE_SINGLETON_FOR_IMPLEMENTATION(DataManager, sharedManager);

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ion" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ion.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -
#pragma mark Profile management
- (NSArray *)getProfiles {
    
    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Profile"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"last_login" ascending:NO];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    return [self.managedObjectContext executeFetchRequest:request error:nil];
}

- (BOOL)setCurrentProfileId:(NSString *)currentProfileId {
    
//    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Profile"];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id = %@", currentProfileId];
//    NSFetchRequest *request = [[NSFetchRequest alloc ] init];
//    [request setEntity:entityDesc];
//    [request setPredicate:predicate];
//
//    NSArray *profiles = [self.managedObjectContext executeFetchRequest:request error:nil];
//    if (profiles && [profiles count] > 0) {
//        profileId = currentProfileId;
//        return YES;
//    }
//
//    return NO;
    profileId = currentProfileId;
    return YES;
}

- (Profile *)getCurrentProfile {

    if (profileId && [profileId length]) {
        NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Profile"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id = %@", profileId];
        NSFetchRequest *request = [[NSFetchRequest alloc ] init];
        [request setEntity:entityDesc];
        [request setPredicate:predicate];
    
        NSArray *profiles = [self.managedObjectContext executeFetchRequest:request error:nil];
        if (profiles && [profiles count] > 0)
            return [profiles firstObject];
    }
    
    return nil;
}


#pragma mark - 
#pragma mark Import contacts
// Check the authorization status of our application for Address Book
-(void)checkAddressBookAccess
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case kABAuthorizationStatusAuthorized:
            break;
            
            // Prompt the user for access to Contacts if there is no definitive answer
        case kABAuthorizationStatusNotDetermined :
            break;
            
            // Display a message if the user has denied or restricted access to Contacts
        case kABAuthorizationStatusDenied:
        case kABAuthorizationStatusRestricted:
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Privacy Warning"
                                                                           message:@"Permission was not granted for Contacts."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        }
            break;
            
        default:
            break;
    }
}

- (NSString*)getCountryCode {
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    return [carrier isoCountryCode];
}

// TODO: 1. check if the contact exists
- (void)importContacts {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    CFErrorRef errRef = NULL;
    ABAddressBookRef abref = ABAddressBookCreateWithOptions(NULL, &errRef);
    ABAddressBookRequestAccessWithCompletion(abref, ^(bool granted, CFErrorRef error) {
        if (granted) {
            NSLog(@"got ab");
            NSArray *contacts = (__bridge NSArray *)(ABAddressBookCopyArrayOfAllPeople(abref));
            NSString *countryCode = [self getCountryCode];
            NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
            NSError *err = nil;
            
            for (int i = 0; i < [contacts count]; i++) {
                ABRecordRef record = (__bridge ABRecordRef)[contacts objectAtIndex:i];
                ABMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
                
                for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
                    NSString* phoneNumber = (__bridge_transfer NSString*) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                    NBPhoneNumber *nbNumber = [phoneUtil parse:phoneNumber
                                                 defaultRegion:countryCode
                                                         error:&err];
                    NSLog(@"%@", phoneNumber);
                    if (err == nil) {
                        if ([phoneUtil isValidNumber:nbNumber]) {
                            NSString *parsedNumber = [phoneUtil format:nbNumber
                                                          numberFormat:NBEPhoneNumberFormatE164
                                                                 error:&err];
                            NSLog(@"%@", parsedNumber);
                            [array addObject:parsedNumber];
                        }
                    }
                }
                CFRelease(phoneNumbers);
            }
            
        } else {
            NSLog(@"ab permission not granted");
        }
    });
    
}

#pragma mark - 
#pragma mark Get local cache
- (NSManagedObject *)getCache:(NSString*)type cache_id:(NSString *)cache_id obj:(PFObject *)obj {

    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:type];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"obj_id = %@", cache_id];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    
    NSError *err = nil;
    NSArray *objs = [self.managedObjectContext executeFetchRequest:request error:&err];
    if (objs && [objs count]) {
        return [objs firstObject];
    } else if (obj) {
        // TODO: fault tolerance
        if ([type  isEqual: @"Participant"]) {
            return [self cacheParticipant:obj.objectId];
        } else if ([type  isEqual: @"Post"]) {
            return [self cachePost:obj];
        }
    }
    
    return nil;
    
}

#pragma mark -
#pragma mark Option related

- (Option *)getOption:(NSString *)option_name {
    
    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Option"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = ", option_name];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    
    NSError *err = nil;
    NSArray *options = [self.managedObjectContext executeFetchRequest:request error:&err];
    if (options && [options count]) {
        return [options firstObject];
    } else {
        return [self cacheOption:option_name];
    }
    
    return nil;
    
}

- (Option *)cacheOption:(NSString *)option_name {
    
    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Option"];
    Option *option = [[Option alloc] initWithEntity:entityDesc
                     insertIntoManagedObjectContext:self.managedObjectContext];
    
    [option setName:option_name];
    [option setObj_id:[NSNumber numberWithInt:rand()]];
    
    NSError *error = nil;
    BOOL ret = [[DataManager sharedManager].managedObjectContext save:&error];
    if (!ret) {
        NSLog(@"savePost failed with error (%@)", error);
        return nil;
    }
    
    return option;
}


#pragma mark -
#pragma mark Participant related

- (Participant *)cacheParticipant:(NSString *)participant_id {
    
    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Participant"];
    Participant *participant = [[Participant alloc] initWithEntity:entityDesc
                                    insertIntoManagedObjectContext:self.managedObjectContext];
    
    [participant setValue:participant_id forKey:@"participant_id"];
    
    NSError *error = nil;
    BOOL ret = [[DataManager sharedManager].managedObjectContext save:&error];
    if (!ret) {
        NSLog(@"savePost failed with error (%@)", error);
        return nil;
    }
    
    return participant;
}

#pragma mark -
#pragma mark Post related

- (Post *)cachePost:(PFObject*)obj {

    PFUser *myself = [PFUser currentUser];
    
    NSEntityDescription *postEntityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Post"];
    Post *post = [[Post alloc] initWithEntity:postEntityDesc
               insertIntoManagedObjectContext:self.managedObjectContext];

    NSMutableArray *participants = [[NSMutableArray alloc] init];
    for (PFObject *pf_participant in obj[@"participant"]) {
        Participant *participant = (Participant *)[self getCache:@"Participant"
                                                        cache_id:pf_participant.objectId
                                                             obj:pf_participant];
// TODO: fault tolerance
        if (participant)
            [participants addObject:participant];
    }
    
    if ([participants count] > 0) {
        
        [post setObj_id:obj.objectId];
        [post setText:obj[@"text"]];
        [post setExpiration:obj[@"expiration"]];
        
        // if not user?
        PFUser *post_user = obj[@"user"];
        Participant *poster = (Participant *)[self getCache:@"Participant"
                                                   cache_id:post_user.objectId
                                                        obj:post_user];
        [post setIssued_by:poster];
        
        int post_owner = (myself.objectId == post_user.objectId)?MY_POST_MASK:FRIEND_POST_MASK;
        [post setStatus:[NSNumber numberWithInt:(POST_STATUS_SENT | post_owner)]];
        [post addHas_participants:[NSSet setWithArray:participants]];
        
        NSError *error = nil;
        BOOL ret = [[DataManager sharedManager].managedObjectContext save:&error];
        if (ret) {
            return post;
        } else {
            NSLog(@"savePost failed with error (%@)", error);
        }
    }
    
    return nil;
}

- (void)getPostsWithBlock:(NSDate*)fromDate toDate:(NSDate*)toDate block:(PostResultBlock)block {

    PFUser *user = [PFUser currentUser];

    // get the latest expiration date of local cached posts
    NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"Post"];
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"expiration" ascending:NO];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDesc]];
    [request setFetchLimit:1];
    
    NSError *err = nil;
    NSArray *posts = [self.managedObjectContext executeFetchRequest:request error:&err];
    
    if (posts && [posts count]) {
        Post *post = [posts firstObject];
        fromDate = post.expiration;
    }
    
    PFQuery *query = [PFQuery queryWithClassName:@"Post"];
    [query whereKey:@"participant" equalTo:user];
    [query whereKey:@"createdAt" lessThanOrEqualTo:toDate];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:fromDate];
    [query orderByAscending:@"expiredAt"];
    NSLog(@"query %@", query);
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *posts, NSError *error) {
        if (!error) {
            NSLog(@"Successfully retrieved %ld posts.", posts.count);
            if (block) {
                NSMutableArray *dataArray = [[NSMutableArray alloc] init];
                // the result is sorted by the expiration date ascendingly,
                // if any record is not in the local cache and its following records shall not in the local cache
                BOOL cached = YES;
                for (PFObject *object in posts) {
                    if (!cached) {
                        [self cachePost:object];
                    } else {
                        cached = ([self getCache:@"Post"
                                        cache_id:object.objectId
                                             obj:nil] == nil);
                        if (!cached) {
                            [self cachePost:object];
                        }
                    }
                }
                block(dataArray, error);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        if (block)
            block(posts, error);
    }];
                        
 }

- (void)getRecentPostsWithBlock:(PostResultBlock)block {

    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = -(kRECENT_POST_DURATION);
    
    NSDate *fromDate = [gregorian dateByAddingComponents:components
                                                  toDate:[NSDate date]
                                                 options:0];
    
    [self getPostsWithBlock:fromDate
                     toDate:[NSDate date]
                      block:block];
}

#pragma mark - Uesr information
- (void)getUserInfoWithBlock:(NSString*)userId block:(UserResultBlock)block {
    
    if (userId && [userId length]) {
        NSEntityDescription *entityDesc = [[self.managedObjectModel entitiesByName] valueForKey:@"User"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id = %@", userId];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        [request setPredicate:predicate];
        
        NSError *err = nil;
        NSArray *profiles = [self.managedObjectContext executeFetchRequest:request error:&err];
        if (profiles && [profiles count] > 0) {
            User *user = [profiles firstObject];
            if (block) {
                block(user, nil);
            }
        } else {
            PFQuery *query = [PFQuery queryWithClassName:@"User"];
            [query whereKey:@"objectId" equalTo:userId];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    NSLog(@"Successfully retrieved %ld users.", objects.count);
                    // Do something with the found objects
                    PFUser *pfuser = [objects firstObject];
                    
                    NSManagedObject *userInfo = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                                                              inManagedObjectContext:self.managedObjectContext];
                    [userInfo setValue:userId forKey:@"user_id"];
                    [userInfo setValue:pfuser[@"name"] forKey:@"nickname"];
                    NSError *saveError = nil;
                    if (![self.managedObjectContext save:&saveError]) {
                        // TODO: the failure of saving local cache should be handled
                        NSLog(@"Failed to save local cache of user (%@) : %@", userId, saveError);
                    }
                    if (block) {
                        block((User *)userInfo, nil);
                    }
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        
        }
    } else {
        if (block) {
//            block(nil, [NSError errorWithDomain:<#(NSString *)#> code:<#(NSInteger)#> userInfo:<#(NSDictionary *)#>])
        }
    }
    
}


#pragma mark - 
#pragma mark Send Parse Notifications
- (void)associateCurrentUsertoInstallation:(PFUser*)user {
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"user"] = user;
    [installation saveInBackground];
}

- (void) sendParseNotificationWithBlock:(NSArray *)receivers data:(NSDictionary *)data block:(PushNotificationResultBlock)block {
    
    PFPush *push = [[PFPush alloc] init];
    [push setChannel:@"Ion"];
    [push setData:data];
    
    PFQuery *query = [PFInstallation query];
    [query whereKey:@"user" containedIn:receivers];
    [push setQuery:query];
    
    [push sendPushInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"Successfully send a push notification");
        } else {
            NSLog(@"Failed to send push notifications out!");
            NSLog(@"%@", error);
        }
        if (block)
            block(succeeded, error);
    }];
    
}


@end
