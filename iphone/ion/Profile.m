//
//  Profile.m
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "Profile.h"


@implementation Profile

@dynamic birthday;
@dynamic email;
@dynamic fb_id;
@dynamic gender;
@dynamic icon;
@dynamic last_login;
@dynamic nickname;
@dynamic user_id;

@end
