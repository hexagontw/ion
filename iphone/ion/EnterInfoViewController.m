//
//  EnterInfoViewController.m
//  ion
//
//  Created by Lawrence on 7/24/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "EnterInfoViewController.h"
#import <Parse/Parse.h>

@interface EnterInfoViewController ()

@end

@implementation EnterInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ok:(id)sender {
    
    if ([self.txtName.text length] == 0) {
        [self.txtName becomeFirstResponder];
        return;
    } else if ([self.txtEmail.text length] == 0) {
        [self.txtEmail becomeFirstResponder];
        return;
    }

    PFUser *user = [PFUser currentUser];
    
    user.email = self.txtEmail.text;
    [user setValue:self.txtName.text forKey:@"name"];
    
    [user saveInBackground];
    [self performSegueWithIdentifier:@"go2ImportContactsViewFromEnterInfo" sender:self];
}

- (IBAction)skip:(id)sender {
    
    if ([self.txtName.text length] == 0) {
        [self.txtName becomeFirstResponder];
        return;
    }
    
    PFUser *user = [PFUser currentUser];
    
    [user saveInBackground];
    
    [self performSegueWithIdentifier:@"go2ImportContactsViewFromEnterInfo" sender:self];
}

@end
