//
//  SignUpViewController.m
//  ion
//
//  Created by Lawrence on 7/24/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "SignUpViewController.h"
#import <Parse/Parse.h>

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)register:(id)sender {
    
    [(UIButton*)sender setEnabled:NO];
    
    if ([self.txtNumber.text length] == 0) {
        [self.txtNumber becomeFirstResponder];
        return;
    }
    
    PFUser *user = [PFUser user];
    user.username = [self.txtNumber text];
    user.password = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hooray! Let them use the app now.
            NSLog(@"Sign up successfull");
            [self performSegueWithIdentifier:@"go2EnterInfoView" sender:self];
        } else {
            NSString *errorString = [error userInfo][@"error"];
            // Show the errorString somewhere and let the user try again.
            [(UIButton *)sender setEnabled:YES];
        }
    }];
    
}

@end
