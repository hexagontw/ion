//
//  Participant.h
//  ion
//
//  Created by Lawrence on 8/11/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post, Response;

@interface Participant : NSManagedObject

@property (nonatomic, retain) NSString * obj_id;
@property (nonatomic, retain) NSSet *answer;
@property (nonatomic, retain) NSSet *issue;
@property (nonatomic, retain) NSSet *join;
@end

@interface Participant (CoreDataGeneratedAccessors)

- (void)addAnswerObject:(Response *)value;
- (void)removeAnswerObject:(Response *)value;
- (void)addAnswer:(NSSet *)values;
- (void)removeAnswer:(NSSet *)values;

- (void)addIssueObject:(Post *)value;
- (void)removeIssueObject:(Post *)value;
- (void)addIssue:(NSSet *)values;
- (void)removeIssue:(NSSet *)values;

- (void)addJoinObject:(Post *)value;
- (void)removeJoinObject:(Post *)value;
- (void)addJoin:(NSSet *)values;
- (void)removeJoin:(NSSet *)values;

@end
