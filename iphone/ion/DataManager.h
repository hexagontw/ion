//
//  DataManager.h
//  ion
//
//  Created by Lawrence on 7/20/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SingletonTemplate.h"

@class Profile, User;
@class PFUser;

typedef void (^PostResultBlock)(NSArray *posts, NSError *error);
typedef void (^PushNotificationResultBlock)(BOOL successed, NSError *error);

typedef void (^UserResultBlock)(User *user, NSError *error);

@interface DataManager : NSObject

SYNTHESIZE_SINGLETON_FOR_INTERFACE(DataManager, sharedManager);

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;

- (NSArray *)getProfiles;
- (BOOL)setCurrentProfileId:(NSString *)profileId;
- (Profile *)getCurrentProfile;

- (void)importContacts;

// parse related
- (void)associateCurrentUsertoInstallation:(PFUser*)user;

- (void)getUserInfoWithBlock:(NSString*)userId block:(UserResultBlock)block;

- (void)getPostsWithBlock:(NSDate*)fromDate toDate:(NSDate*)toDate block:(PostResultBlock)block;
- (void)getRecentPostsWithBlock:(PostResultBlock)block;

@end

