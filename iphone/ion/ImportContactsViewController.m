//
//  ImportContactsViewController.m
//  ion
//
//  Created by Lawrence on 7/24/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "ImportContactsViewController.h"
#import "DataManager.h"

@interface ImportContactsViewController ()
{
    NSMutableArray *dataArray;    
    
}

@end

@implementation ImportContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Import Contacts"
                                                    message:@"Ion will check your contacts and import the user who has ion's account."
                                                   delegate:self
                                          cancelButtonTitle:@"Not now"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%ld", buttonIndex);
}

#pragma mark - 
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"phonenumber"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"phonenumber"];
    }
    
    cell.textLabel.text = [dataArray objectAtIndex:indexPath.item];
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0)
        return [dataArray count];
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return @"Voting";
    } else {
        return @"Voted";
    }
    
}

#pragma mark -
#pragma mark UITableViewDelegate






- (IBAction)ok:(id)sender {
    
}

@end
