//
//  PickVoterViewController.m
//  ion
//
//  Created by Lawrence on 7/14/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "PickVoterViewController.h"
#import "VoterCell.h"
#import "UIUser.h"

@interface PickVoterViewController ()
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation PickVoterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataArray = [[NSMutableArray alloc] init];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -
#pragma mark UITableViewDataSource

//| ----------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (NSInteger)[self.dataArray count];
}

//| ----------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *kVoterCellID = @"VoterCell";
    
    VoterCell *cell = (VoterCell *)[tableView dequeueReusableCellWithIdentifier:kVoterCellID];
    
    UIUser *uiuser = [self.dataArray objectAtIndex:indexPath.row];
    
    cell.voterImage.image = uiuser.image;
    cell.voterLabel.text = uiuser.user.nickname;
    [cell setSelected:uiuser.checked];
//    cell.checkBox.checked = uiuser.checked;
    
    // Accessibility
    [self updateAccessibilityForCell:cell];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIUser *uiuser = [self.dataArray objectAtIndex:indexPath.row];
    uiuser.checked = (uiuser.checked)?NO:YES;
    
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark -
#pragma mark Accessibility

- (void)updateAccessibilityForCell:(VoterCell *)cell {
    // The cell's accessibilityValue is the Checkbox's accessibilityValue.
    cell.accessibilityValue = (cell.selected) ? @"Enabled" : @"Disabled";
}


#pragma mark - 
#pragma mark PrepareForSegue
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"UnwindVoterPicker"]) {
        return YES;
    }
    
    return NO;
}

// prepare the array of voters
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"UnwindVoterPicker"]) {
        
        self.voters = _dataArray;
    } else {
// TODO: fault tolerance
        NSLog(@"invalid segue");
    }
}

@end
