//
//  Participant.m
//  ion
//
//  Created by Lawrence on 8/11/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "Participant.h"
#import "Post.h"
#import "Response.h"


@implementation Participant

@dynamic obj_id;
@dynamic answer;
@dynamic issue;
@dynamic join;

@end
