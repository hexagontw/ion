//
//  PickVoterViewController.h
//  ion
//
//  Created by Lawrence on 7/14/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickVoterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *voterTableView;

@property (retain, nonatomic) NSArray *voters;

@end
