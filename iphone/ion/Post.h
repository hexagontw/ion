//
//  Post.h
//  ion
//
//  Created by Lawrence on 8/11/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Option, Participant, Response;

#define MY_POST_MASK            0x0100
#define FRIEND_POST_MASK        0x0200

#define POST_STATUS_FAIL        0x1000

#define POST_STATUS_DRAFT       0x0001
#define POST_STATUS_SENDING     0x0002
#define POST_STATUS_SENT        0x0004

#define IS_MY_POST(a) ([a.status intValue] & MY_POST_MASK == MY_POST_MASK)
#define IS_FRIEND_POST(a) ([a.status intValue] & FRIEND_POST_MASK == FRIEND_POST_MASK)
#define IS_POST_FAIL(a) ([a.status intValue] & POST_STATUS_FAIL == POST_STATUS_FAIL)
#define IS_POST_SENT(a) ([a.status intValue] & POST_STATUS_SENT == POST_STATUS_SENT)

@interface Post : NSManagedObject

@property (nonatomic, retain) NSDate * expiration;
@property (nonatomic, retain) NSString * obj_id;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSSet *has_options;
@property (nonatomic, retain) NSSet *has_participants;
@property (nonatomic, retain) NSSet *has_responses;
@property (nonatomic, retain) Participant *issued_by;
@end

@interface Post (CoreDataGeneratedAccessors)

- (void)addHas_optionsObject:(Option *)value;
- (void)removeHas_optionsObject:(Option *)value;
- (void)addHas_options:(NSSet *)values;
- (void)removeHas_options:(NSSet *)values;

- (void)addHas_participantsObject:(Participant *)value;
- (void)removeHas_participantsObject:(Participant *)value;
- (void)addHas_participants:(NSSet *)values;
- (void)removeHas_participants:(NSSet *)values;

- (void)addHas_responsesObject:(Response *)value;
- (void)removeHas_responsesObject:(Response *)value;
- (void)addHas_responses:(NSSet *)values;
- (void)removeHas_responses:(NSSet *)values;

@end
