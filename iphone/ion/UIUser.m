//
//  UIUser.m
//  ion
//
//  Created by Lawrence on 7/16/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "UIUser.h"

@implementation UIUser

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ checked(%d) %@", self.user.nickname, self.checked, self.image];
}

@end
