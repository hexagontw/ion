//
//  NewVoteViewController.h
//  ion
//
//  Created by Lawrence on 7/14/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewVoteViewController : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *txtQuestion;

- (IBAction)unwindtoNewVoteViewController:(UIStoryboardSegue *)segue;

@end
