//
//  Option.m
//  ion
//
//  Created by Lawrence on 8/11/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "Option.h"
#import "Post.h"
#import "Response.h"


@implementation Option

@dynamic name;
@dynamic obj_id;
@dynamic answered_by;
@dynamic used_by;

@end
