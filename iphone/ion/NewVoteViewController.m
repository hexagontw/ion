//
//  NewVoteViewController.m
//  ion
//
//  Created by Lawrence on 7/14/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "NewVoteViewController.h"
#import "DurationViewController.h"
#import "PickVoterViewController.h"


@interface NewVoteViewController ()
{
    BOOL        placeholderShowed;
    UIButton    *btnDone;
    NSDate      *dueDate;
    NSArray     *voters;

}
@end

@implementation NewVoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    placeholderShowed = YES;
    btnDone = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnDone setFrame:CGRectMake(0, 0, self.view.frame.size.width, 36)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor lightGrayColor]];
    [btnDone setAlpha:0.7];
    [btnDone addTarget:self
                action:@selector(done)
      forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView == _txtQuestion) {
        if (placeholderShowed) {
            [textView setText:@""];
            [textView setTextColor:[UIColor blackColor]];
            placeholderShowed = NO;
        }
        [textView setInputAccessoryView:btnDone];
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView == _txtQuestion) {
        if ([[textView text] length] == 0) {
            [textView setText:@"Touch to enter the question"];
            [textView setTextColor:[UIColor lightGrayColor]];
            placeholderShowed = YES;
        }
        [textView setInputAccessoryView:nil];
    }
}


#pragma mark - Done
- (void)done
{
    [_txtQuestion resignFirstResponder];
}

- (IBAction)unwindToMainVoteView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)unwindtoNewVoteViewController:(UIStoryboardSegue *)segue {
    
    if ([segue.identifier isEqualToString:@"UnwindDatePicker"]) {
        DurationViewController *controller = (DurationViewController *)segue.sourceViewController;
        
        dueDate = controller.timePicker.date;
        NSLog(@"date %@", dueDate);
    } else if ([segue.identifier isEqualToString:@"UnwindVoterPicker"]) {
        PickVoterViewController *controller = (PickVoterViewController *)segue.sourceViewController;
        
        voters = controller.voters;
        NSLog(@"voters %@", voters);
    }
    
}

@end
