//
//  User.m
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic icon;
@dynamic nickname;
@dynamic thumbnail;
@dynamic user_id;

@end
