//
//  UIUser.h
//  ion
//
//  Created by Lawrence on 7/16/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UIUser : NSObject
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, readwrite) BOOL checked;

@end
