//
//  VoterCell.h
//  ion
//
//  Created by Lawrence on 7/16/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Checkbox.h"

@interface VoterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *voterImage;
@property (weak, nonatomic) IBOutlet UILabel *voterLabel;
//@property (weak, nonatomic) IBOutlet Checkbox *checkBox;

@end
