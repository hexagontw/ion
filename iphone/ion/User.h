//
//  User.h
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSData * icon;
@property (nonatomic, retain) NSString * nickname;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSString * user_id;

@end
