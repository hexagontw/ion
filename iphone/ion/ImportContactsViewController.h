//
//  ImportContactsViewController.h
//  ion
//
//  Created by Lawrence on 7/24/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportContactsViewController : UIViewController < UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *contractsTableView;
- (IBAction)ok:(id)sender;

@end
