//
//  MainViewController.m
//  ion
//
//  Created by Lawrence on 7/24/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "MainViewController.h"
#import <AddressBook/AddressBook.h>
#import <Parse/Parse.h>
#import "DataManager.h"

@interface MainViewController ()
{
    NSMutableArray *ongoingPosts;
    NSMutableArray *expiredPosts;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor magentaColor];
    [refreshControl addTarget:self
                       action:@selector(onRefresh:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onRefresh:(id)sender {
    NSLog(@"refresh");
    [self reloadPosts];
}

#pragma mark -
#pragma mark Reload Posts via Data Manager

- (void)reloadPosts {
    
    [[DataManager sharedManager] getRecentPostsWithBlock:^(NSArray *posts, NSError *error) {
        
        if (error == nil) {
            [ongoingPosts removeAllObjects];
            [expiredPosts removeAllObjects];
            NSDate *now = [NSDate date];
            for (PFObject *post in posts) {
                if ([post objectForKey:@"expireAt"]) {
                    if ([now compare:post[@"expireAt"]] == NSOrderedAscending)
                        [ongoingPosts addObject:post];
                    else
                        [expiredPosts addObject:post];
                } else {
                    // this is an invalid post ?
                }
            }
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        } else {
            // what should we do if got error here ?
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        }
    }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return ([ongoingPosts count] > 0) ? 2 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"phonenumber"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"phonenumber"];
    }
    
    NSArray *dataSource = ([ongoingPosts count] > 0 && indexPath.section == 0)?ongoingPosts:expiredPosts;
    cell.textLabel.text = [dataSource objectAtIndex:indexPath.item];
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return ([ongoingPosts count] > 0 && section == 0)?[ongoingPosts count]:[expiredPosts count];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if ([ongoingPosts count] > 0 && section == 0)
        return @"On Going";
    
    return @"Expired";
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
