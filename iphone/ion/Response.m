//
//  Response.m
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "Response.h"
#import "Option.h"
#import "Participant.h"
#import "Post.h"


@implementation Response

@dynamic answered_by;
@dynamic in_post;
@dynamic is_option;

@end
