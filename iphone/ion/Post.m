//
//  Post.m
//  ion
//
//  Created by Lawrence on 8/11/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "Post.h"
#import "Option.h"
#import "Participant.h"
#import "Response.h"


@implementation Post

@dynamic expiration;
@dynamic obj_id;
@dynamic status;
@dynamic text;
@dynamic has_options;
@dynamic has_participants;
@dynamic has_responses;
@dynamic issued_by;

@end
