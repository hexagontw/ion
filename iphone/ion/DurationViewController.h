//
//  DurationViewController.h
//  ion
//
//  Created by Lawrence on 7/17/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import "ViewController.h"

@interface DurationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end
