//
//  Profile.h
//  ion
//
//  Created by Lawrence on 8/6/14.
//  Copyright (c) 2014 Hexagon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Profile : NSManagedObject

@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fb_id;
@property (nonatomic, retain) NSDecimalNumber * gender;
@property (nonatomic, retain) NSData * icon;
@property (nonatomic, retain) NSDate * last_login;
@property (nonatomic, retain) NSString * nickname;
@property (nonatomic, retain) NSString * user_id;

@end
